package pl.jurczak.kamil.bean;

import lombok.Data;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named("user")
@RequestScoped
@Data
public class User {

    private String firstName;
    private String lastName;
    private String username;
    private String password;

}
